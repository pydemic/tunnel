FROM python:3.8.2-slim

ENV PYTHONUNBUFFERED=1 \
    HCAP__ENV=prod \
    HCAP__HOST=0.0.0.0

RUN apt-get update && \
    apt-get install -y \
      gcc \
      gettext && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip --no-cache-dir install --upgrade pip && \
    pip --no-cache-dir install --upgrade setuptools

WORKDIR /app
