FROM python:3.8.2

ENV PYTHONUNBUFFERED=1 \
    HCAP__ENV=dev \
    HCAP__HOST=0.0.0.0

RUN apt-get update && \
    apt-get install -y \
      gettext \
      git \
      iproute2 \
      locales \
      lsb-release \
      procps && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip --no-cache-dir install --upgrade pip && \
    pip --no-cache-dir install --upgrade setuptools

WORKDIR /app

CMD [ "sleep", "infinity" ]
