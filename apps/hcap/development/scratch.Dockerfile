FROM pydemic/hcap:python-development

COPY setup.py setup.py

RUN pip install -e .[dev]
