FROM pydemic/hcap:development

COPY setup.py setup.py

RUN pip install -e .[dev]
