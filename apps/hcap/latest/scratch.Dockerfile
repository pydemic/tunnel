FROM pydemic/hcap:python-production

ENV HCAP__ENV=latest

COPY hcap hcap
COPY hcap_accounts hcap_accounts
COPY hcap_geo hcap_geo
COPY hcap_institutions hcap_institutions
COPY hcap_monitors hcap_monitors
COPY hcap_notifications hcap_notifications
COPY hcap_utils hcap_utils
COPY locale locale
COPY manage.py setup.py tasks.py ./

RUN pip install -e .[prod] && \
    hcap collectstatic && \
    hcap compilemessages

CMD [ "hcap", "start" ]
