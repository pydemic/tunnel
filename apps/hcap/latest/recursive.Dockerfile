FROM pydemic/hcap:latest

RUN rm -rf \
      hcap \
      hcap_accounts \
      hcap_geo \
      hcap_institutions \
      hcap_monitors \
      hcap_notifications \
      hcap_utils \
      locale \
      static

COPY hcap hcap
COPY hcap_accounts hcap_accounts
COPY hcap_geo hcap_geo
COPY hcap_institutions hcap_institutions
COPY hcap_monitors hcap_monitors
COPY hcap_notifications hcap_notifications
COPY hcap_utils hcap_utils
COPY locale locale
COPY manage.py setup.py tasks.py ./

RUN pip install -e .[prod] && \
    hcap collectstatic && \
    hcap compilemessages
