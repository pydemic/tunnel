FROM python:3.8.2

ENV PYTHONUNBUFFERED=1 \
    FLIT_ROOT_INSTALL=1

COPY locale.gen /etc/locale.gen

RUN apt-get update && \
    apt-get install -y \
      git \
      iproute2 \
      locales \
      lsb-release \
      procps && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    locale-gen && \
    pip --no-cache-dir install --upgrade pip && \
    pip --no-cache-dir install flit

WORKDIR /app

CMD [ "sleep", "infinity" ]
