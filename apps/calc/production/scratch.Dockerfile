FROM pydemic/calc:python-production

ENV STREAMLIT_BROWSER_GATHER_USAGE_STATS=false \
    STREAMLIT_SERVER_FILE_WATCHER_TYPE=none

COPY covid covid

COPY pyproject.toml tasks.py ./

RUN flit install -s --deps=production && \
    pybabel compile -d covid/locale

CMD [ "inv", "run" ]
