FROM pydemic/calc:production

RUN rm -rf covid

COPY covid covid

COPY pyproject.toml tasks.py ./

RUN flit install -s --deps=production && \
    pybabel compile -d covid/locale
