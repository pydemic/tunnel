FROM node:13.12.0 as node
FROM python:3.8.2-slim

COPY --from=node /usr/local /usr/local
COPY --from=node /opt /opt

ENV PYTHONUNBUFFERED=1

RUN apt-get update && \
    apt-get install -y \
      g++ \
      git \
      make \
      libprotobuf-dev \
      protobuf-compiler \
      python2 \
      rsync && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip --no-cache-dir install --upgrade pip && \
    pip --no-cache-dir install mypy-protobuf streamlit && \
    rm -rf /usr/local/lib/python3.8/site-packages/streamlit && \
    git clone https://github.com/streamlit/streamlit.git \
      /usr/local/lib/python3.8/site-packages/streamlit && \
    cd /usr/local/lib/python3.8/site-packages/streamlit && \
      make all-devel && \
      make frontend

WORKDIR /usr/local/lib/python3.8/site-packages/streamlit
